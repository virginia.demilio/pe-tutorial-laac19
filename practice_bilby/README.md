# bilby_pipe quick-start guide

## Installation

For general running, preferably use the released conda packages (on, e.g., CIT):

```bash
$ source /cvmfs/ligo-containers.opensciencegrid.org/lscsoft/conda/latest/etc/profile.d/conda.sh
$ conda activate ligo-py37
```

You can check if things are working by running

```bash
(ligo-py37) 18:25 ~: $ bilby_pipe --version
18:25 bilby INFO    : Running bilby version: 0.5.5
18:25 bilby_pipe INFO    : Running bilby_pipe version: 0.2.5
```

## ini files

The `bilby_pipe` package takes as input an `ini` files, for example

```ini
trigger-time = 1126259462.3910  # Trigger time from G184098

label = dynesty_IMRPhenomPV2_GW150914
outdir = no_calibration
accounting = ligo.dev.o3.cbc.pe.lalinference

detectors = [H1, L1]
duration = 4
coherence-test = False
channel-dict = {H1:DCS-CALIB_STRAIN_C02, L1:DCS-CALIB_STRAIN_C02}
psd-dict = {H1:./h1_psd.dat, L1:./l1_psd.dat}

maximum-frequency=1024
minimum-frequency=20
sampling-frequency=4096
reference-frequency = 20
waveform-approximant = IMRPhenomPV2
frequency-domain-source-model = lal_binary_black_hole

prior_file = GW150914.prior
deltaT = 0.2

```

The prior is specified by the prior file

```ini
mass_ratio = Uniform(name='mass_ratio', minimum=0.125, maximum=1, boundary='reflective')
chirp_mass = Uniform(name='chirp_mass', minimum=9.0, maximum=69.9, unit='$M_{\odot}$', boundary='reflective')
mass_1 = Constraint(name='mass_1', minimum=10, maximum=80)
mass_2 = Constraint(name='mass_2', minimum=10, maximum=80)
a_1 = Uniform(name='a_1', minimum=0, maximum=0.99, boundary='reflective')
a_2 = Uniform(name='a_2', minimum=0, maximum=0.99, boundary='reflective')
tilt_1 = Sine(name='tilt_1', boundary='reflective')
tilt_2 = Sine(name='tilt_2', boundary='reflective')
phi_12 = Uniform(name='phi_12', minimum=0, maximum=2 * np.pi, boundary='periodic')
phi_jl = Uniform(name='phi_jl', minimum=0, maximum=2 * np.pi, boundary='periodic')
luminosity_distance = PowerLaw(alpha=2, name='luminosity_distance', minimum=50, maximum=2000, unit='Mpc', latex_label='$d_L$')
dec =  Cosine(name='dec', boundary='reflective')
ra =  Uniform(name='ra', minimum=0, maximum=2 * np.pi, boundary='periodic')
theta_jn =  Sine(name='theta_jn', boundary='reflective')
psi =  Uniform(name='psi', minimum=0, maximum=np.pi, boundary='periodic')
phase =  Uniform(name='phase', minimum=0, maximum=2 * np.pi, boundary='periodic')
```

## Getting help

There is [documentation](https://lscsoft.docs.ligo.org/bilby_pipe/user-interface.html), but first:

```bash
$ bilby_pipe --help
```

After that https://chat.ligo.org/ligo/channels/bilby-help


## Example


* [Example of a run directory for running GW150914](https://ldas-jobs.ligo.caltech.edu/~gregory.ashton/bilby_review/calibration_review/bilby/)