{
  "nbformat": 4,
  "nbformat_minor": 0,
  "metadata": {
    "colab": {
      "name": "Bayesian inference with Bilby.ipynb",
      "version": "0.3.2",
      "provenance": [],
      "collapsed_sections": []
    },
    "kernelspec": {
      "name": "python3",
      "display_name": "Python 3"
    }
  },
  "cells": [
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "sB_vemQ3F9EY",
        "colab_type": "text"
      },
      "source": [
        "# Bayesian Inference with Bilby\n",
        "## Where to get help\n",
        "\n",
        "Project repository: https://git.ligo.org/lscsoft/bilby\n",
        "\n",
        "Example library: https://git.ligo.org/lscsoft/bilby/tree/master/examples\n",
        "(Good place to get started if you find an example that is close to what you want to do)\n",
        "\n",
        "Documentation: https://lscsoft.docs.ligo.org/bilby/\n",
        "\n",
        "Bilby help on Mattermost: https://chat.ligo.org/ligo/channels/bilby-help\n",
        "\n",
        "Bilby Slack: https://bilby-code.slack.com/ (Invitation on request, post questions on the #help channel)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "VFYLDKLaKb8r",
        "colab_type": "text"
      },
      "source": [
        "## Please ask questions during the tutorial!\n",
        "\n",
        "![alt text](https://i.imgur.com/b1GCMHH.png)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "_USuOgNGOdAh",
        "colab_type": "text"
      },
      "source": [
        "## What does Bilby roughly look like?\n",
        "![alt text](https://i.imgur.com/rlNaUch.png)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "MM9EorBJ3meP",
        "colab_type": "text"
      },
      "source": [
        "## Tutorial Setup\n",
        "An example of how to use bilby to perform paramater estimation for\n",
        "non-gravitational wave data consisting of a Gaussian with a mean and variance.\n",
        "\n",
        "### Setup on Google Colab or on your own machine\n",
        "\n",
        "Run the following line below:\n"
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "vrm7nHyRlxwl",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "!pip install bilby ptemcee nestle emcee cpnest"
      ],
      "execution_count": 0,
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "s8B1aaCflsnS",
        "colab_type": "text"
      },
      "source": [
        "### Setup on CIT via Jupyter Lab\n",
        "You need to change to the right environment. Click on the grey circle next to `Python 3` on the top right on your page and the select the `ligo-py37` environment.\n",
        "\n",
        "![alt text](https://i.imgur.com/ngHqVaB.png)\n",
        "\n",
        "![alt text](https://i.imgur.com/kwH6d5g.png)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "OKZsbcFcoG-K",
        "colab_type": "text"
      },
      "source": [
        "## Imports"
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "4wXC8Zlg3HQA",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "import bilby\n",
        "import numpy as np\n",
        "import matplotlib.pyplot as plt\n",
        "\n",
        "%pylab inline"
      ],
      "execution_count": 0,
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "eSDT5GYJBEnH",
        "colab_type": "text"
      },
      "source": [
        "## Priors in Bilby\n",
        "\n",
        "Priors in Bilby are implemented as classes in Python. Using an object-oriented approach helps us doing a lot of things dynamically in the background without the user having to worry about them.\n",
        "\n",
        "We implemented a range of different classes of priors into bilby. We are going to look at a power law prior of the form:\n",
        "\n",
        "$\\pi(\\theta) \\propto \\theta^{\\alpha}$\n",
        "\n",
        "Between a given minimum and maximum allowed value for $\\theta$.\n",
        "\n",
        "Let's instantiate a prior!\n"
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "Ddwx8QT1BD0u",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "power_law_prior = bilby.core.prior.PowerLaw(minimum=1, maximum=5, alpha=-3)"
      ],
      "execution_count": 0,
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "mUJWEfLJZums",
        "colab_type": "text"
      },
      "source": [
        "Note that Bilby takes care of the normalization in the background for us.\n",
        "\n",
        "If you are not familiar with object orientation in Python, take a moment to look at the next cell. **Objects** in Python have **attributes** that you can access and set with the '.' operator."
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "wuJOd61zY-bM",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "print('The minimum of the prior is ' + str(power_law_prior.minimum))\n",
        "print('The maximum of the prior is ' + str(power_law_prior.maximum))\n",
        "print('The spectral index of the prior is ' + str(power_law_prior.alpha))"
      ],
      "execution_count": 0,
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "vN1RM3iLa4wM",
        "colab_type": "text"
      },
      "source": [
        "The prior object also has a number of **methods**. Since a prior is a probability distribution, we have implemented the typical properties of such a distribution.\n",
        "\n",
        "You can evaluate the probability density at any given value, calculate the cumulative density function, draw random samples from the distribution, etc."
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "N-4mr-PYP_lJ",
        "colab_type": "text"
      },
      "source": [
        "### Probability density function\n",
        "\n",
        "We can get the prior probability at any given value by using the `prob` method. Let's evaluate the prior at some value."
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "K0cswskpQgl7",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "power_law_prior.prob(val=2.4)"
      ],
      "execution_count": 0,
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "IemYMDGxQlpO",
        "colab_type": "text"
      },
      "source": [
        "Let's also see if the probability distribution looks sensible if we plot it."
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "GUlkZP5-NX1m",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "grid = np.linspace(1.0, 5.0, 100)\n",
        "plt.plot(grid, power_law_prior.prob(grid))\n",
        "plt.loglog()\n",
        "plt.xlabel('$\\\\theta$')\n",
        "plt.ylabel('probability density')\n",
        "plt.show()\n",
        "plt.savefig('prior_density')\n",
        "plt.clf()"
      ],
      "execution_count": 0,
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "-z5BVsLtYX7S",
        "colab_type": "text"
      },
      "source": [
        "This looks like a Power Law distribution."
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "ELS5z-TkYrZt",
        "colab_type": "text"
      },
      "source": [
        "### Cumulative Distribution Function\n",
        "\n",
        "Let's continue with the cumulative distribution function (`cdf`). We go through the same two steps to convince ourselves that this looks sensible. The `cdf` of the prior $\\pi(\\theta)$ is defined as.\n",
        "\n",
        "$\\mathrm{CDF}(\\theta) = \\int_{\\theta_{\\mathrm{min}}}^{\\theta}\\pi(\\theta ') d\\theta '$"
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "RbcZxWCAZLWb",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "power_law_prior.cdf(val=2.4)"
      ],
      "execution_count": 0,
      "outputs": []
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "CpCF1mwcRZ3v",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "grid = np.linspace(1.0, 5.0, 100)\n",
        "plt.plot(grid, power_law_prior.cdf(grid))\n",
        "plt.xlabel('$\\\\theta$')\n",
        "plt.ylabel('Cumulative Distribution')\n",
        "plt.show()\n",
        "plt.savefig('prior_cdf')\n",
        "plt.clf()"
      ],
      "execution_count": 0,
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "IODSyO3EZkmm",
        "colab_type": "text"
      },
      "source": [
        "### Sampling from the prior\n",
        "\n",
        "In the sampling process we need to draw samples from the priors. For this we implemented the `sample` method. Let's start by drawing a single sample from the prior distribution."
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "OVQFl765OWtA",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "power_law_prior.sample(size=1)"
      ],
      "execution_count": 0,
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "yUFkX5m3a5iA",
        "colab_type": "text"
      },
      "source": [
        "Let's now draw 10000 samples and bin them. We also redo the steps from the **Probability distribution** section to check if the samples match up with the analytic distribution."
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "lD4uDgirPzxH",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "# Binning\n",
        "samples = power_law_prior.sample(size=10000)\n",
        "plt.hist(samples, bins='fd', density=True, label='Binned samples');\n",
        "\n",
        "# Analytic pdf\n",
        "grid = np.linspace(1.0, 5.0, 100)\n",
        "plt.plot(grid, power_law_prior.prob(grid), label='Analytic pdf')\n",
        "\n",
        "plt.loglog()\n",
        "plt.legend()\n",
        "plt.xlabel('$\\\\theta$')\n",
        "plt.ylabel('probability density')\n",
        "plt.show()\n",
        "plt.savefig('prior_sampled')\n",
        "plt.clf()"
      ],
      "execution_count": 0,
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "B2aPkxU1EspM",
        "colab_type": "text"
      },
      "source": [
        "#### Exercise (in your own time)\n",
        "\n",
        "Try the above steps with some of the other bilby priors. You can use the templates below, just fill in the misssing parameter values.\n",
        "\n",
        "`bilby.core.prior.DeltaFunction(peak=)`\n",
        "\n",
        "`bilby.core.prior.Uniform(minimum=, maximum=)`\n",
        "\n",
        "`bilby.core.prior.LogUniform(minimum=, maximum=)`\n",
        "\n",
        "`bilby.core.prior.Cosine(minimum=-np.pi/2, maximum=np.pi/2)`\n",
        "\n",
        "`bilby.core.prior.Gaussian(mu=, sigma=)`\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "bhyuK5wwd0mI",
        "colab_type": "text"
      },
      "source": [
        "## Likelihoods in Bilby\n",
        "\n",
        "We now want to look at how we can implement a likelihood in Bilby. We already provide a number of analytic likelihoods and an implementation for gravitational-wave transients, but it is instructive to see how we can implement one for ourselves.\n",
        "\n",
        "In order to implement a custom likelihood into bilby, you need to define a class that **inherits** from the base likelihood class in Bilby.\n",
        "\n",
        "Let's have a look at the implementation below."
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "sFL-fmxN3yZU",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "class SimpleGaussianLikelihood(bilby.Likelihood):\n",
        "    def __init__(self, data):\n",
        "        \"\"\"\n",
        "        A very simple Gaussian likelihood\n",
        "\n",
        "        Parameters\n",
        "        ----------\n",
        "        data: array_like\n",
        "            The data to analyse\n",
        "        \"\"\"\n",
        "        super(SimpleGaussianLikelihood, self).__init__(parameters={'mu': None, 'sigma': None})\n",
        "        self.data = data\n",
        "        self.N = len(data)\n",
        "\n",
        "    def log_likelihood(self):\n",
        "        mu = self.parameters['mu']\n",
        "        sigma = self.parameters['sigma']\n",
        "        res = self.data - mu\n",
        "        return -0.5 * (np.sum((res / sigma)**2) +\n",
        "                       self.N * np.log(2 * np.pi * sigma**2))"
      ],
      "execution_count": 0,
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "mxUiwFJggww4",
        "colab_type": "text"
      },
      "source": [
        "We need to implement an `__init__` method which is called upon instantiation. This takes in our data and defines the two parameters that are relevant for our problem, $\\mu$ and $\\sigma$.\n",
        "\n",
        "Additionally, we implemented the `log_likelihood` method for normal distributed data, which is defined as:\n",
        "\n",
        "$\\ln \\mathcal{L} = -\\frac{1}{2} \\left(\\sum_{i=1}^N \\left(\\frac{x_i-\\mu}{\\sigma}\\right)^2 - N \\ln (2\\pi\\sigma^2)\\right)$\n",
        "\n",
        "Here the $x_i$ are our data points sampled from a normal distribution, $\\mu$ and $\\sigma$ are the parameters that we want to estimate."
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "gcZi_E1SiKpF",
        "colab_type": "text"
      },
      "source": [
        "## Running a simple example\n",
        "\n",
        "### Data creation\n",
        "Let us now consider a simple example in which we recover the mean and the standard deviation from normally distributed data. We start doing this by generating some data."
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "_pcqBKUq_s-v",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "mu = 3\n",
        "sigma = 4\n",
        "data = np.random.normal(mu, sigma, 10000)"
      ],
      "execution_count": 0,
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "AHHXyL6vCBvY",
        "colab_type": "text"
      },
      "source": [
        "Let's have a look at our data first."
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "h37g5b9ICGuz",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "plt.hist(data, bins='fd')\n",
        "plt.xlabel('Value')\n",
        "plt.ylabel('Count')\n",
        "plt.show()\n",
        "plt.savefig('normal_distributed_data')\n",
        "plt.clf()"
      ],
      "execution_count": 0,
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "KlbYF1Q0iur-",
        "colab_type": "text"
      },
      "source": [
        "### Setting up likelihoods and priors\n",
        "This looks good. We now set up our likelihood that we defined earlier."
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "Zts9Wroti8d7",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "likelihood = SimpleGaussianLikelihood(data=data)"
      ],
      "execution_count": 0,
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "E4_G0r485JAA",
        "colab_type": "text"
      },
      "source": [
        "#### Excercise (in your own time)\n",
        "\n",
        "Evaluate the likelihood for a few different values of `mu` and `sigma`.\n",
        "\n",
        "Hint: You can set the parameter of the likelihood like this\n",
        "\n",
        "`likelihood.parameters['mu'] = 8`"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "_DlAVqIzjVOr",
        "colab_type": "text"
      },
      "source": [
        "Let's continue by setting up the priors. Since we have multiple parameters, we need to bundle them together. In Bilby, we have implemented a `PriorDict` as a container to do this. The `PriorDict` can be used the same way as any normal python `dict`, but it also implements the `prob`, `cdf`, `sample` (and a lot more) methods of the `Prior` class. \n",
        "\n",
        "We also add `latex_labels` to our priors. These will be used in our plots later.\n",
        "\n",
        "Here is how you can set this up:\n"
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "NfUN1UmW4Waf",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "priors = bilby.core.prior.PriorDict()\n",
        "priors['mu'] = bilby.core.prior.Uniform(minimum=0, maximum=10, latex_label='$\\mu$')\n",
        "priors['sigma'] = bilby.core.prior.Uniform(minimum=0, maximum=10, latex_label='$\\sigma$')"
      ],
      "execution_count": 0,
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "AeRP40cllXdq",
        "colab_type": "text"
      },
      "source": [
        "#### Excercise (in your own time)\n",
        "\n",
        "Try out the `prob`, `sample`, `cdf` methods of the `PriorDict`.\n",
        "\n",
        "Hint: `prob` and `cdf` now take a `sample` argument, that is a dict with the parameter values."
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "FrapXUiWgsFm",
        "colab_type": "text"
      },
      "source": [
        "### Running inference\n",
        "\n",
        "We can use different samplers in Bilby thanks to the interfaces we built. All the samplers are accessed via the `run_sampler` function.\n",
        "\n",
        "The `run_sampler` function takes the defined `likelihood` and `priors` objects, `outdir` and `label` arguments to define where we want our output to go.\n",
        "Additionally, it takes all the keyword arguments that are present in the sampler. For example, for dynesty we define the number of live points `npoints`."
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "k7GZPPTS40UR",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "result = bilby.run_sampler(\n",
        "    likelihood=likelihood, priors=priors, sampler='dynesty', npoints=500, \n",
        "    walks=10, outdir='outdir', label='dynesty_run', resume=False)"
      ],
      "execution_count": 0,
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "gnHN1zGNiFXq",
        "colab_type": "text"
      },
      "source": [
        "### Displaying the results\n",
        "\n",
        "The result object in Bilby contains all relevant information from the run. It is automatically stored at the end of the run. Most of the time, we are interested in the posterior distribution of the parameters. We can see this by using the `plot_corner` method."
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "C5nsTcXEiGYt",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "result.plot_corner(truths=dict(mu=mu, sigma=sigma))\n"
      ],
      "execution_count": 0,
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "X6aIZKMlnM1g",
        "colab_type": "text"
      },
      "source": [
        "#### Excercise (in your own time)\n",
        "\n",
        "Re-run the sampling and plotting step with `ptemcee` and `nestle` instead of `dynesty` as your sampler. By saving the results in different variables, you can plot multiple posteriors like this:\n",
        "\n",
        "`bilby.core.result.plot_multiple([result_1, result_2, result_3])`\n",
        "\n",
        "Hint: You need to replace `npoints` with `nwalkers` for `ptemcee`. `ptemcee` also does not use the `walks` keyword argument. "
      ]
    }
  ]
}